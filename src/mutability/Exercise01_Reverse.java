package mutability;

public class Exercise01_Reverse {
     /*
    Write a method that takes a String and returns it back reversed

    Hello -> olleH
    Java -> avaJ
      ->
    a  -> a
    1234 -> 4321
    Good morning -> gninrom dooG
      */

    public static String reverseString(String str){


        String name = "Jeffrey";

        return new StringBuilder(str).reverse().toString();


//        StringBuilder reversed = new StringBuilder();
//
//        for (int i = str.length()-1; i >= 0; i--) {
//            reversed.append(str.charAt(i));
//        }
//
//        return reversed.toString();
    }

    /*
    Write a method that takes a String and returns true/false
    return true if it is palindrome
    return false if it is not palindrome

    civic -> true
    madam -> true
    hello -> false
     */

    public static boolean isPalindrome(String str){

        return reverseString(str).equals(str);
        //return str.equals(new StringBuilder().reverse().toString());
    }

    public static void main(String[] args) {

        System.out.println(reverseString("Hello"));
        System.out.println(reverseString("Java"));
        System.out.println(reverseString("a"));
        System.out.println(reverseString("1234"));


        System.out.println(isPalindrome("civic"));
        System.out.println(isPalindrome("madam"));
        System.out.println(isPalindrome("hello"));
    }
}
