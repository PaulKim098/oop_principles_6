package mutability;

public class Exercise02_InjectStart {
    /*
    Create a method that takes a String and returns a String
    if the given String have even length and length is at least 2, then insert * in the middle of the String
    aa -> a*a
    java -> ja*va
    toyota -> toy*ota

    if the given String has odd length is at least 3, then insert * in before and after the middle character
    aaa -> a*a*a
    hello -> he*l*lo

    ""      -> ""
    "a"     -> ""
    "aa"    -> "a*a"
    "aaa"   -> "a*a*a"

     */

    public static String insertStar(String str){
        if(str.length() <= 1) return str;

        //the length at least 2
        else if(str.length() % 2 == 0){ // a*a bb*bb
            return new StringBuilder(str).insert(str.length()/2, "*").toString();
        }
        else{ // 3,5,7
            return new StringBuilder(str).insert(str.length()/2, "*").insert(str.length()/2+2, "*").toString();
        }

//        if(str.length() <= 1) return "";
//        else if(str.length() % 2 == 0) return  str.substring(0, str.length()/2) + "*" + str.substring(str.length()/2);
//        return str.substring(0, str.length()/2) + "*" + str.charAt(str.length()/2) + "*" + str.substring(str.length()/2+1);
    }

    public static void main(String[] args) {
        System.out.println(insertStar(""));
        System.out.println(insertStar("a"));
        System.out.println(insertStar("aa"));
        System.out.println(insertStar("aaaaa"));
        System.out.println(insertStar("hello"));
        System.out.println(insertStar("java"));
        System.out.println(insertStar("bass"));

    }
}
