import java.util.ArrayList;
import java.util.TreeSet;

public class Mock3 {
    public static void main(String[] args) {
        /*
        -Write a public static method that takes an int array as an argument and returns an int array.
        -The method should multiply each element with 2 and return it back.
        -Method name can be called as multiply2.
         */

        System.out.println(multiply2(new int[] {3, 5, 0, 1}));

    }

    public static int[] multiply2(int[] arr){


        for (int i = 0; i < arr.length; i++) {
            arr[i] *= 2;
        }

        return arr;


    }

    public static int maxNumber(ArrayList<Integer> list){

        return new TreeSet<>(list).last();



    }

    /*
    Requirement:
-Write a public static method that takes a String as an argument and returns an int.
-The method should find the some of all digits in the String and return it back.
-Method name can be called as findDigitSum.
NOTE: If given String is empty or have no digits then return zero.
     */


    /*
    public static int[] uniques(int[] n){
        HashSet<Integer> a = new HashSet<>(); // container to hold uniques (non-dups)
        for (int element : n) {
            a.add(element);
        }
        int[] newArray = new int[a.size()];
        int i = 0;
        for (Integer element : a){
            newArray[i++] = element;
        }
        return newArray;
    }
^^ here is an example of how to use for each loop with an Array. However, recall we only use for each if were going to transverse through the entire list from beginning to end.
int i = 0 --> line is setting first element index value so it can be resigned
newArray[i++] --> this is increasing that 0 with each loop so that the other indexes can be reassigned as it loops (edited)
take a moment and think about what is happening in each iteration.
     */

}
