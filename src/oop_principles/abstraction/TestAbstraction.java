package oop_principles.abstraction;

public class TestAbstraction {
    public static void main(String[] args) {
        // Phone phone = new Phone(); Abstract class cannot be instantiated

        Samsung s1 = new Samsung();
        IPhone i1 = new IPhone();
        Nokia n1 = new Nokia();

        s1.call();
        i1.call();
        n1.call();

        s1.text();
        i1.text();
        n1.text();

        s1.ring();
        i1.ring();
        n1.ring();

        s1.connectWifi();
        i1.connectWifi();

    }
}
