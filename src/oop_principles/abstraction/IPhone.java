package oop_principles.abstraction;

public class IPhone extends Phone implements Wifi, Camera, Bluetooth{

    @Override
    public void call() {
        System.out.println("iPhone calls");
    }

    @Override
    public void text() {
        System.out.println("iPhone texts");
    }

    @Override
    public void ring(){
        System.out.println("iPhone rings");
    }

    @Override
    public void connectWifi() {
        System.out.println("Iphone connects Wifi");
    }

    @Override
    public void Camera() {
        System.out.println("iPhone has Camera");
    }

    @Override
    public void recordVideo() {
        System.out.println("iPhone records Video");
    }

    @Override
    public void connectBluetooth() {
        System.out.println("iPhone connects Bluetooth");
    }
}
