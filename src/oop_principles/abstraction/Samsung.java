package oop_principles.abstraction;

public class Samsung extends Phone implements Wifi, Camera, Bluetooth{

    public Samsung(){}

    public Samsung(String color){
        super(color);
    }

    @Override
    public void call() {
        System.out.println("Samsung calls");
    }

    @Override
    public void text() {
        System.out.println("Samsung texts");
    }

    @Override
    public void connectWifi() {
        System.out.println("Samsung connects Wifi");
    }


    @Override
    public void Camera() {
        System.out.println("Samsung uses Camera");
    }

    @Override
    public void recordVideo() {
        System.out.println("Samsung records Video");
    }

    @Override
    public void connectBluetooth() {
        System.out.println("Samsung connects Bluetooth");
    }
}
