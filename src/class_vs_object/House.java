package class_vs_object;

public class House {


    //default constructor
    public House(){

    }

    //5-args constructor - overloaded
    public House(String color, double price, int year, String address, boolean hasGarden){
        this.color = color;
        this.price = price;
        this.year = year;
        this.address = address;
        this.hasGarden = hasGarden;
    }

    //2-args constructor
    public House(double price, int year) {
        this.price = price;
        this.year = year;
    }

    //instance variable - features that defines a House object (instance)
    public String color = "White";
    public double price;
    public int year;
    public String address;
    public boolean hasGarden = true;


    @Override
    public String toString() {
        return "House{" +
                "color='" + color + '\'' +
                ", price=" + price +
                ", year=" + year +
                ", address='" + address + '\'' +
                ", hasGarden=" + hasGarden +
                '}';
    }

}
