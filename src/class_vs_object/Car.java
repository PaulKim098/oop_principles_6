package class_vs_object;

public class Car {

    //Create 4 instance variables as make, model, year, price and make them all public
    public String make;
    public String model;
    public int year;
    public double price;

    //Create 4-args constructor
    public Car(String make, String model, int year, double price) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.price = price;
    }

    //Override toString method
    @Override
    public String toString() {
        return "Car{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", price=" + price +
                '}';
    }


}
