package class_vs_object;

public class Test {
    public static void main(String[] args) {
        String s = "Hello World"; // eoo

        //Count vowels
        System.out.println(s.replaceAll("[^aeiouAEIOU]", "").length());
    }
}
