package garbage_collection;

import class_vs_object.Animal;

public class UnderstandingGC {
    public static void main(String[] args) {

        Animal animal1 = new Animal();
        Animal animal2 = new Animal();

        System.out.println(animal1); // class_vs_object.Animal@1b6d3586
        System.out.println(animal2); // class_vs_object.Animal@4554617c

        animal1 = animal2; // Line that garbage collection happens
        System.gc();        // Garbage Collection normally is running implicitly, but we can use the gc method in System or Runtime class
        Runtime.getRuntime().gc();

        System.out.println(animal1); // class_vs_object.Animal@4554617c
        System.out.println(animal2); // class_vs_object.Animal@4554617c

    }
}
