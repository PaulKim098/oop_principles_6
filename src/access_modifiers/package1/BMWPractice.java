package access_modifiers.package1;

class BMWPractice { //Accessible only in the same package and the same class

    private BMWPractice(){

    }


    public static void main(String[] args) {
        BMW b1 = new BMW();
        BMW b2 = new BMW();
    }
}
